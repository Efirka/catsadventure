﻿using System.Windows.Forms;

namespace CatsAdventure
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.AllPanel = new System.Windows.Forms.TableLayoutPanel();
            this.upPanel = new System.Windows.Forms.TableLayoutPanel();
            this.mini = new System.Windows.Forms.Button();
            this.end = new System.Windows.Forms.Button();
            this.gamePanel = new System.Windows.Forms.TableLayoutPanel();
            this.Raskaz = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.AllPanel.SuspendLayout();
            this.upPanel.SuspendLayout();
            this.gamePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.buttonPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // AllPanel
            // 
            this.AllPanel.ColumnCount = 1;
            this.AllPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.AllPanel.Controls.Add(this.upPanel, 0, 0);
            this.AllPanel.Controls.Add(this.gamePanel, 0, 1);
            this.AllPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AllPanel.Location = new System.Drawing.Point(0, 0);
            this.AllPanel.Margin = new System.Windows.Forms.Padding(0);
            this.AllPanel.Name = "AllPanel";
            this.AllPanel.RowCount = 2;
            this.AllPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.AllPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.AllPanel.Size = new System.Drawing.Size(346, 568);
            this.AllPanel.TabIndex = 0;
            // 
            // upPanel
            // 
            this.upPanel.BackColor = System.Drawing.Color.Black;
            this.upPanel.ColumnCount = 3;
            this.upPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.upPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.upPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.upPanel.Controls.Add(this.mini, 1, 0);
            this.upPanel.Controls.Add(this.end, 2, 0);
            this.upPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.upPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.upPanel.Location = new System.Drawing.Point(0, 0);
            this.upPanel.Margin = new System.Windows.Forms.Padding(0);
            this.upPanel.Name = "upPanel";
            this.upPanel.RowCount = 1;
            this.upPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.upPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.upPanel.Size = new System.Drawing.Size(346, 30);
            this.upPanel.TabIndex = 0;
            this.upPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.upPanel_MouseDown);
            this.upPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.upPanel_MouseMove);
            // 
            // mini
            // 
            this.mini.BackColor = System.Drawing.Color.Black;
            this.mini.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mini.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mini.FlatAppearance.BorderSize = 0;
            this.mini.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.mini.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mini.ForeColor = System.Drawing.Color.White;
            this.mini.Location = new System.Drawing.Point(286, 0);
            this.mini.Margin = new System.Windows.Forms.Padding(0);
            this.mini.Name = "mini";
            this.mini.Size = new System.Drawing.Size(30, 30);
            this.mini.TabIndex = 5;
            this.mini.Text = "";
            this.mini.UseVisualStyleBackColor = false;
            this.mini.Click += new System.EventHandler(this.mini_Click);
            // 
            // end
            // 
            this.end.BackColor = System.Drawing.Color.Black;
            this.end.Cursor = System.Windows.Forms.Cursors.Hand;
            this.end.Dock = System.Windows.Forms.DockStyle.Fill;
            this.end.FlatAppearance.BorderSize = 0;
            this.end.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.end.Font = new System.Drawing.Font("Segoe MDL2 Assets", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.end.ForeColor = System.Drawing.Color.White;
            this.end.Location = new System.Drawing.Point(316, 0);
            this.end.Margin = new System.Windows.Forms.Padding(0);
            this.end.Name = "end";
            this.end.Size = new System.Drawing.Size(30, 30);
            this.end.TabIndex = 3;
            this.end.Text = "";
            this.end.UseVisualStyleBackColor = false;
            this.end.Click += new System.EventHandler(this.end_Click);
            // 
            // gamePanel
            // 
            this.gamePanel.ColumnCount = 1;
            this.gamePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gamePanel.Controls.Add(this.Raskaz, 0, 1);
            this.gamePanel.Controls.Add(this.pictureBox1, 0, 0);
            this.gamePanel.Controls.Add(this.buttonPanel, 0, 2);
            this.gamePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gamePanel.Location = new System.Drawing.Point(0, 30);
            this.gamePanel.Margin = new System.Windows.Forms.Padding(0);
            this.gamePanel.Name = "gamePanel";
            this.gamePanel.RowCount = 3;
            this.gamePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 346F));
            this.gamePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.gamePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.gamePanel.Size = new System.Drawing.Size(346, 538);
            this.gamePanel.TabIndex = 1;
            // 
            // Raskaz
            // 
            this.Raskaz.BackColor = System.Drawing.Color.Black;
            this.Raskaz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Raskaz.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Raskaz.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Raskaz.ForeColor = System.Drawing.Color.White;
            this.Raskaz.Location = new System.Drawing.Point(0, 346);
            this.Raskaz.Margin = new System.Windows.Forms.Padding(0);
            this.Raskaz.Name = "Raskaz";
            this.Raskaz.Size = new System.Drawing.Size(346, 192);
            this.Raskaz.TabIndex = 11;
            this.Raskaz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::CatsAdventure.Properties.Resources.kadr0;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(346, 346);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // buttonPanel
            // 
            this.buttonPanel.ColumnCount = 2;
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.buttonPanel.Controls.Add(this.button2, 0, 0);
            this.buttonPanel.Controls.Add(this.button1, 0, 0);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPanel.Location = new System.Drawing.Point(0, 538);
            this.buttonPanel.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.RowCount = 1;
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.buttonPanel.Size = new System.Drawing.Size(346, 1);
            this.buttonPanel.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(173, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(173, 1);
            this.button2.TabIndex = 2;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.MouseEnter += new System.EventHandler(this.button2_MouseEnter);
            this.button2.MouseLeave += new System.EventHandler(this.button2_MouseLeave);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(173, 1);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 568);
            this.Controls.Add(this.AllPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.AllPanel.ResumeLayout(false);
            this.upPanel.ResumeLayout(false);
            this.gamePanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.buttonPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private TableLayoutPanel AllPanel;
        private TableLayoutPanel upPanel;
        public Button end;
        public Button mini;
        private TableLayoutPanel gamePanel;
        public Label Raskaz;
        public PictureBox pictureBox1;
        private TableLayoutPanel buttonPanel;
        public Button button2;
        public Button button1;
    }
}

