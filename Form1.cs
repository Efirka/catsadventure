﻿//using CatsAdventure.ButtonController;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CatsAdventure
{
    public partial class Form1 : Form
    {
        public Button but1;
        public Button but2;
        public Label lab;
        public PictureBox pict;
        public TableLayoutPanel buttForm;
        
        public Form1()
        {
            InitializeComponent();
            istoria = new Logica(this);
            but1 = button1;
            but2 = button2;
            lab = Raskaz;
            pict = pictureBox1;
            buttForm = gamePanel;
        }


        Logica istoria;
        public void pictureBox1_Click(object sender, EventArgs e)
        {
            
            istoria.Start(shag, syjet, v);
            istoria.Suzhet(shag, syjet, v);
            shag++;
        }

        #region музыка


        SoundPlayer simpleSound;
        private void Form1_Load(object sender, EventArgs e)
        {

            string music = string.Format(@"{0}\Music\main.wav", Application.StartupPath);
            simpleSound = new SoundPlayer(music);
            simpleSound.PlayLooping();
            MessageBox.Show("Механика игры заключается в том, чтобы нажимать на картинку для смены текста. В случае если вам нужно сделать выбор, нажмите на кнопку, а затем на картинку.");

        }

        #endregion

        #region кнопки вверху
        Form mainForm;
        private void end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mini_Click(object sender, EventArgs e)
        {
            mainForm.WindowState = FormWindowState.Minimized;
        }
        #endregion

        #region перемещение приложения
        
        Point lastPoint;

        private void upPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }


        private void upPanel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }




        #endregion

        #region цвет нижних кнопок
        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.ForeColor = Color.DarkGray;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.ForeColor = Color.White;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.ForeColor = Color.DarkGray;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.ForeColor = Color.White;
        }

        #endregion




        int syjet = 0;
        int shag = 0;
        int v = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (shag == 5) v = 1;
            //UNDONE: сделать переход сразу на следующую картинку    

            else if (shag == 9 && v == 1) syjet = 1;
            else if (shag == 13 && v == 1) syjet = 3;
            else if (shag == 15 && v == 1) { shag = 13; syjet = 3; }
            else if (shag == 19 && v == 1) syjet = 5;
            else if (shag == 26 && v == 1) syjet = 9;

            else if (shag == 9 && v == 2) syjet = 1;
            else if (shag == 13 && v == 2) syjet = 3;
            else if (shag == 14 && v == 2) syjet = 5;
            else if (shag == 17 && v == 2) syjet = 7;
            else if (shag == 20 && v == 2) syjet = 9;
            else if (shag == 23 && v == 2) syjet = 13;
            else if (shag == 22 && v == 2) { shag = 13; syjet = 3; v = 1; }


            //концовки
            else if (shag == 17 && v == 1) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 21 && v == 1) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 28 && v == 1) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 30 && v == 1) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 33 && v == 1) { shag = 0; buttForm.RowStyles[2].Height = 0; }

            else if (shag == 18 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 19 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 21 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 27 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 26 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 30 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }
            else if (shag == 33 && v == 2) { shag = 0; buttForm.RowStyles[2].Height = 0; }

            istoria.Suzhet(shag, syjet, v);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (shag == 5) v = 2;  
            else if (shag == 9 && v == 1) syjet = 2; 
            else if (shag == 13 && v == 1) syjet = 4;  
            else if (shag == 15 && v == 1) syjet = 6; 
            else if (shag == 19 && v == 1) syjet = 8; 
            else if (shag == 26 && v == 1) syjet = 10;

            else if (shag == 9 && v == 2) syjet = 2;
            else if (shag == 13 && v == 2) syjet = 4;
            else if (shag == 14 && v == 2) syjet = 6;
            else if (shag == 17 && v == 2) syjet = 8;
            else if (shag == 20 && v == 2) syjet = 10;
            else if (shag == 22 && v == 2) syjet = 12;
            else if (shag == 23 && v == 2) syjet = 14;
            




            //концовки
            else if (shag == 17 && v == 1) Application.Exit();
            else if (shag == 21 && v == 1) Application.Exit();
            else if (shag == 28 && v == 1) Application.Exit();
            else if (shag == 30 && v == 1) Application.Exit();
            else if (shag == 33 && v == 1) Application.Exit();

            else if (shag == 18 && v == 2) Application.Exit();
            else if (shag == 19 && v == 2) Application.Exit();
            else if (shag == 21 && v == 2) Application.Exit();
            else if (shag == 27 && v == 2) Application.Exit();
            else if (shag == 26 && v == 2) Application.Exit();
            else if (shag == 30 && v == 2) Application.Exit();
            else if (shag == 33 && v == 2) Application.Exit();

            istoria.Suzhet(shag, syjet, v);
        }

        public Logica Logica
        {
            get => default;
            set
            {
            }
        }
    }

}
